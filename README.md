# User CRUD System

This project has the functionality to register a new user, login, change user data and remove user. It was based on its development ReactJS and nodeJS.

### 📋 Requirements

node must be at version >= 14.0.0

```
node -v
```

Install yarn

```
npm install --global yarn
```

## 🚀 Getting Started

[Download the project](https://gitlab.com/guilherme199213/orangefox/-/archive/main/orangefox-main.zip).

After downloading, extract the downloaded file. Then access the generated folder path and run the following command at the prompt:

```
yarn
```

When finished, run the following command at the prompt:

```
yarn start
```

## 🛠️ Developed with

* [ReactJS](https://pt-br.reactjs.org/docs/getting-started.html) - Framework Front-End
* [nodeJS](https://nodejs.org/en/docs/) - Frameword Back-End
* [mongoDB](https://docs.mongodb.com/) - Database

## ⚡ To facilitate

access [Orangefox](https://orangefox.idealizou.com.br/) to view the demo. Log in using the following credentials:

* **For admin**
email: admin@gmail.com
password: 123456

* **For customer**
email: cliente@gmail.com
password: 123456

## ✒️ Developer

* **Guilherme Mendes** - *Full-Stack* - [Profile](https://gitlab.com/guilherme199213)