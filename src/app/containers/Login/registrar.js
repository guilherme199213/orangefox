import React, { Component } from 'react';
import Titulo from '../../components/Texto/Titulo';
import Input from '../../components/Inputs/Simples';
import Checkbox from '../../components/Inputs/Checkbox';
import Button from '../../components/Button/Simples';
import '../../../css/index.css'
import { Link } from 'react-router-dom'

import { connect } from 'react-redux';
import * as actions from '../../actions'
import { api, versao } from '../../config'

class Registrar extends Component {
    state = {
        nome: '',
        email: '',
        password: '',
        opcaoLembrar: true,
        erros: {}
    }

    onChangeInput = (field, ev) => {
        this.setState({ [field]: ev.target.value })
        this.validate()
    }
    onChangeCheckBox = (field) => this.setState({ [field]: !this.state[field] })

    registrarUsuario() {
        const { email, password, nome } = this.state;
        if (!this.validate()) return;
        this.props.registrarUsuario({ email, password, nome }, (error) => {
            this.setState({ erros: { ...this.state.erros, form: error } })
        })
    }

    validate() {
        const { email, password, nome } = this.state;
        const erros = {};
        if (!nome) erros.nome = 'Preencha aqui com seu nome'
        if (!email) erros.email = 'Preencha aqui com seu e-mail'
        if (!email.includes('@')) erros.email = 'Preencha corretamente seu e-mail'
        if (!password) erros.password = 'Preencha aqui com sua senha'
        this.setState({ erros });
        return !(Object.keys(erros).length > 0);
    }

    render() {
        const { email, password, nome, opcaoLembrar, erros } = this.state;
        return (
            <div className='Login flex flex-center'>
                <div className='Card'>
                    <div className='flex vertical flex-center'>
                        <Titulo tipo='h1' titulo='Orangefox' />
                        <p>Faça seu cadastro abaixo</p>
                    </div>
                    <br /><br />

                    <Input
                        label='Nome'
                        value={nome}
                        type='nome'
                        error={erros.nome}
                        onChange={(ev) => this.onChangeInput('nome', ev)} />

                    <Input
                        label='E-mail'
                        value={email}
                        type='email'
                        error={erros.email}
                        onChange={(ev) => this.onChangeInput('email', ev)} />

                    <Input
                        label='Senha'
                        value={password}
                        error={erros.password}
                        type='password'
                        onChange={(ev) => this.onChangeInput('password', ev)} />

                    <div className='flex'>
                        <div className='flex-1 flex'>
                            <Link to='/'>
                                <small>Faça login</small>
                            </Link>
                        </div>
                        <div className='flex-1 flex flex-end'>
                            <a href={`${api}/${versao}/api/usuarios/recuperar-senha`}>
                                <small>Esqueceu sua senha?</small>
                            </a>
                        </div>
                    </div>
                    <br /><br />
                    <div className='flex flex-center'>
                        <Button type='success' label='Cadastrar' onClick={() => this.registrarUsuario()} />
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(null, actions)(Registrar);