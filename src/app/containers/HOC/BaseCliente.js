import React from 'react';
import BaseCliente from '../BaseCliente';
import { connect } from 'react-redux';
import * as actions from '../../actions'

const baseCliente = Component => {
    class ComponentBaseCliente extends React.Component {

        componentDidMount() {
            const { getUser, authorized, history, usuario } = this.props;
            getUser();
            if (!authorized || !usuario || !usuario.role.includes('cliente')) history.replace('/');
        }
        componentDidUpdate(prevProps) {
            const { history } = this.props;
            if (!this.props.authorized ||
                !this.props.usuario ||
                !this.props.usuario.role.includes('cliente')) {
                history.replace('/');
            }

        }

        render() {
            return (
                <BaseCliente history={this.props.history}>
                    <Component {...this.props} />
                </BaseCliente>
            )
        }
    }

    const mapStateToProps = state => ({
        authorized: state.auth.authorized,
        usuario: state.auth.usuario
    });

    return connect(mapStateToProps, actions)(ComponentBaseCliente)
}

export default baseCliente;
