import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions'

const noAuth = Component => {
    class ComponentNoAuth extends React.Component {

        async componentDidMount() {
            const { getUser, authorized, history, usuario } = this.props;
            await getUser();
            if (authorized && usuario.role.includes('adm')) history.replace('/adm/perfil');
            if (authorized && usuario.role.includes('cliente')) history.replace('/cliente');
        }
        componentDidUpdate(prevProps) {
            const { authorized, history } = prevProps;
            if (!authorized && this.props.authorized && this.props.usuario.role.includes('adm')) history.replace('/adm/perfil');
            if (!authorized && this.props.authorized && this.props.usuario.role.includes('cliente')) history.replace('/cliente');
        }

        render() {
            return (
                <div>
                    <Component {...this.props} />
                </div>
            )
        }
    }

    const mapStateToProps = state => ({
        authorized: state.auth.authorized,
        usuario: state.auth.usuario
    });

    return connect(mapStateToProps, actions)(ComponentNoAuth)
}

export default noAuth;
