import React from 'react'

const Header = ({ handleLogout }) => (
    <div className="Header flex horizontal full-width">
        <div className="flex-1 flex flex-start">
            <a href='/'>Ambiente CLIENTE</a>
        </div>
        <div className='flex-1 flex flex-end'>
            <span onClick={() => handleLogout()}>Sair</span>
        </div>
    </div>
)

export default Header;
