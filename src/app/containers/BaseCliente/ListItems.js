import React from 'react';
import { Link } from 'react-router-dom';

const items = [
    { rota: '/', icone: (<i className="fas fa-user" />), titulo: 'Minha Conta' },
    { rota: '/', icone: (<i className="fas fa-shopping-basket" />), titulo: 'Meus Pedidos' },
    { rota: '/', icone: (<i className="fas fa-thumbs-up" />), titulo: 'Avaliações' },
    { rota: '/', icone: (<i className="fas fa-headphones" />), titulo: 'Atendimento' },
    { rota: '/', icone: (<i className="fas fa-heart" />), titulo: 'Favoritos' }
]

const ListItems = ({ open, history }) => {
    const localAtual = history.location.pathname;

    return (
        (
            <div className="items-wrapper">
                {
                    items.map((item, idx) => (
                        <Link to={item.rota} key={idx}>
                            <div className={`menu-item ${localAtual === item.rota ? 'menu-item-active' : ''} flex horizontal`}>
                                <div className="flex-1 flex flex-center">
                                    {item.icone}
                                </div>
                                {open && (
                                    <div className="flex-2 flex flex-start">
                                        <span>{item.titulo}</span>
                                    </div>)}
                            </div>
                        </Link>
                    ))
                }
            </div>
        )
    )
}

export default ListItems
