import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './store';

import { BrowserRouter as Router, Route } from 'react-router-dom'

import { initApp } from './actions'

import baseAdm from './containers/HOC/BaseAdm';
import baseCliente from './containers/HOC/BaseCliente';
import noAuth from './containers/HOC/NoAuth';


// CONTAINER COM BASE
import Login from './containers/Login';
import AreaAdm from './containers/AreaAdm';
import PerfilAdm from './containers/AreaAdm/perfil';
import AreaCliente from './containers/AreaCliente';
import registrar from './containers/Login/registrar';

export default class App extends Component {

    componentDidMount() {
        initApp();
    }

    render() {
        return (
            <Provider store={store}>
                <Router>
                    <div className="App">
                        <Route path={'/'} exact component={noAuth(Login)} />
                        <Route path={'/registrar'} exact component={noAuth(registrar)} />
                        <Route path={'/adm'} exact component={baseAdm(AreaAdm)} />
                        <Route path={'/adm/perfil'} exact component={baseAdm(PerfilAdm)} />
                        <Route path={'/cliente'} exact component={baseCliente(AreaCliente)} />
                    </div>
                </Router>
            </Provider>
        );
    }
}
