import axios from 'axios';
import {
    LOGIN_USER,
    LOGOUT_USER,
    REMOVE_USUARIO
} from './types';
import { api, versao } from '../config';
import { saveToken, getHeaders, cleanToken } from './localStorage';
import errorHandling from "./errorHandling";

export const initApp = () => {
    const opcaoLembrar = localStorage.getItem("opcaoLembrar");
    if (opcaoLembrar === "false") cleanToken();
}

// USUARIOS
export const handleLogin = ({ email, password, opcaoLembrar }, callback) => {
    return function (dispatch) {
        axios.post(`${api}/${versao}/api/usuarios/login`, { email, password })
            .then((response) => {
                saveToken(response.data.usuario, opcaoLembrar);
                dispatch({ type: LOGIN_USER, payload: response.data });
                // window.location.href = '/adm'
            })
            .catch((e) => callback(errorHandling(e)));
    }
}

export const registrarUsuario = ({ email, password, nome }, callback) => {
    return function (dispatch) {
        axios.post(`${api}/${versao}/api/usuarios/registrar`, { email, password, nome })
            .then((response) => {
                saveToken(response.data.usuario);
                dispatch({ type: LOGIN_USER, payload: response.data });
            })
            .catch((e) => callback(errorHandling(e)));
    }
}

export const getUser = () => {
    return function (dispatch) {
        axios.get(`${api}/${versao}/api/usuarios/`, getHeaders())
            .then((response) => {
                saveToken(response.data.usuario, true);
                dispatch({ type: LOGIN_USER, payload: response.data });
            })
            .catch((error) => console.log(error, error.response, error.response && error.response.data));
    }
}

export const updateUser = (dados, cb) => {
    return function (dispatch) {
        axios.put(`${api}/${versao}/api/usuarios/`, dados, getHeaders())
            .then((response) => {
                saveToken(response.data.usuario, true);
                dispatch({ type: LOGIN_USER, payload: response.data });
                cb(null);
            })
            .catch((error) => cb(errorHandling(error)));
    }
}

export const removerUsuario = (cb) => {
    return function (dispatch) {
        axios.delete(`${api}/${versao}/api/usuarios/`, getHeaders())
            .then(response => {
                dispatch(({ type: REMOVE_USUARIO, payload: response.data }));
                cb(null);
            })
            .catch((e) => cb(errorHandling(e)));
    }
}



export const handleLogout = () => {
    cleanToken();
    return { type: LOGOUT_USER };
}

